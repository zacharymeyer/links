# links

[![pipeline status](https://gitlab.com/zacharymeyer/links/badges/main/pipeline.svg)](https://gitlab.com/zacharymeyer/links/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

A Link tree like site for my personal social accounts.

[View the live site](https://links.zacharymeyer.io)
